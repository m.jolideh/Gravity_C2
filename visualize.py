"""PLOT(Traj,inData,nframes,finterval) simulates the motion of two bodies whose trajectories are defined by array, and whose masses are defined in inData.
Traj is an array of 4 rows and n columns 

nframes is a scalar representing the number of frames for the simulation (that is the number of steps)
finterval is a scalar representing the interval between successive frames   
"""  


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#import engin as en

def PLOT(Traj,inData):
    #Trajectories of bodies    
    x1=Traj[:,0]; 
    y1=Traj[:,1]; 
    x2=Traj[:,2]; 
    y2=Traj[:,3];

    #Masses of bodies
    m1=inData[0,1];
    m2=inData[1,1]; 

    #limits of x and y axes
    maxX=max([max(abs(x1)), max(abs(x2))])*1.2
    maxY=max([max(abs(y1)), max(abs(y2))])*1.2

    #Prepare window to make simulation 
    fig, ax = plt.subplots()
    ax.set_xlim(-maxX,maxX)
    ax.set_ylim(-maxY,maxY)
    line1, = ax.plot([], [], 'b.', ms=m1)  #ms is mass (here, size) of the particle
    line2, = ax.plot([], [], 'r.', ms=m2)   
    def animate(t):
        line1.set_data(x1[t-1],y1[t-1])  #data from engine is updated here 
        line2.set_data(x2[t-1],y2[t-1]) 
        return line1, line2

    ani = animation.FuncAnimation(fig, animate, interval=2, blit=True, frames=200000)
	
    plt.show()

#PLOT(Traj,inData,nframes,finterval)



