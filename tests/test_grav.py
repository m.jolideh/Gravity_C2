
''' In this file I have prepared several tests that check if our group project is 
    working properly. The tests that I have is as following:
    1) It checkes if the all individual parts work!
    2) It tests if the input file is correctly imported!
    3) It tests that the distance between two particles not be zero!
'''
import numpy
from main_gravity import main,data_loader 
from nose.tools import with_setup, raises


#python engin.py

def test_main_returns_greetings():
    """tests to see if there is any inconsistancy in the main file"""
    assert main() == "correct execution"


#def test_input_data():
#    """tests that check if the correct file has been inputted"""
#    print numpy.loadtxt("Initial_values.txt")
#    assert data_loader() ==  numpy.loadtxt("Initial_values.txt") 

#def test_initial_value():
#    'tests that the initial value of different particles can not be the same'
#    assert r !=0 

#def test_energy_conserv():
#    "tests that the energy should be conserved"
#    assert E == E[0]
    

