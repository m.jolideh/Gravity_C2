import numpy as np

#def acc()

acc=1
Data = np.array([[1.,1.,1.,1.,1.,1.],[2.,1.,2.,2.,2.,2.]])
dt=np.float32(0.0001)

def update_velocityverlet(x_v, acc):
    """Update the positions and velocities by using velocityverlet"""
    """dt=time step (local variable)"""      
    #Data=x_v
    #position = Data[:,2:4]
    #velocity =Data[:,4:6]   
    

    #acc = force_calc(x_v[:,2:6])
    x_v[:,2]=x_v[:,2]+(0.5*(dt*dt)*acc)+x_v[:,4]*dt
    x_v[:,3]=x_v[:,3]+(0.5*(dt*dt)*acc)+x_v[:,5]*dt
    x_v[:,4]+=dt*acc
    x_v[:,5]+=dt*acc
    return x_v

print(update_velocityverlet(Data, acc))
