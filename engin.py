
'''  
GRAVITY 2
this program takes the initial conditions and gives the position of 2 masses at each time step

'''

import numpy as np
import math
import matplotlib.pyplot as plt
STEP=20000
G= 1. # 6.67*(10**(-11))
dt=.01
time = np.arange(0,(STEP*dt),dt)

Data = np.loadtxt("Initial_values.txt")
#print (Data)
def Traject(Data):

    
    m1,x1_i,y1_i,vx1_i,vy1_i,m2,x2_i,y2_i,vx2_i,vy2_i = Data[0,1],Data[0,2],Data[0,3],Data[0,4],Data[0,5],Data[1,1],Data[1,2],Data[1,3],Data[1,4],Data[1,5]  
    
    '''
    Create a list for each variable in problem like position -x  for each mass 

    '''

    x1=[0]*STEP
    y1=[0]*STEP
    x2=[0]*STEP
    y2=[0]*STEP
    vx1=[0]*STEP
    vx2=[0]*STEP
    vy1=[0]*STEP
    vy2=[0]*STEP

    ax1=[0]*STEP
    ax2=[0]*STEP
    ay1=[0]*STEP
    ay2=[0]*STEP
    
    '''
    take the initial conditions as the first element of above lists
    ''' 
    
    x1[0]=x1_i
    y1[0]=y1_i
    vx1[0]=vx1_i
    vy1[0]=vy1_i
    x2[0]=x2_i
    y2[0]=y2_i
    vx2[0]=vx2_i
    vy2[0]=vy2_i
    
    '''
    calculte the initial acceleration of each mass
    
    ''' 
    ax1[0]=(G*m2/((x1[0]-x2[0])**2+(y1[0]-y2[0])**2)**1.5)*(x2[0]-x1[0])
    ax2[0]=(G*m1/((x1[0]-x2[0])**2+(y1[0]-y2[0])**2)**1.5)*(x1[0]-x2[0])
    ay1[0]=(G*m2/((x1[0]-x2[0])**2+(y1[0]-y2[0])**2)**1.5)*(y2[0]-y1[0])
    ay2[0]=(G*m1/((x1[0]-x2[0])**2+(y1[0]-y2[0])**2)**1.5)*(y1[0]-y2[0])

    
   ''' 
    Update postion and velocity using Verlet velocity algorithm
   
   '''
    for i in range(STEP-1):
        #x1[i+1]=vx1[i]*dt +x1[i]
        x2[i+1]=vx2[i]*dt +x2[i]
        #y1[i+1]=vy1[i]*dt +y1[i]
        y2[i+1]=vy2[i]*dt +y2[i]

        ax1[i+1]=(G*m2/((x1[i+1]-x2[i+1])**2 +(y1[i+1]-y2[i+1])**2)**1.5)*(x2[i+1]-x1[i+1])
        ax2[i+1]=(G*m1/((x1[i+1]-x2[i+1])**2 +(y1[i+1]-y2[i+1])**2)**1.5)*(x1[i+1]-x2[i+1])
        ay1[i+1]=(G*m2/((x1[i+1]-x2[i+1])**2 +(y1[i+1]-y2[i+1])**2)**1.5)*(y2[i+1]-y1[i+1])
        ay2[i+1]=(G*m1/((x1[i+1]-x2[i+1])**2 +(y1[i+1]-y2[i+1])**2)**1.5)*(y1[i+1]-y2[i+1])



        #vx1[i+1]=vx1[i]+1./2*(ax1[i]+ax1[i+1])*dt
        vx2[i+1]=vx2[i]+1./2*(ax2[i]+ax2[i+1])*dt
        #vy1[i+1]=vy1[i]+1./2*(ay1[i]+ay1[i+1])*dt
        vy2[i+1]=vy2[i]+1./2*(ay2[i]+ay2[i+1])*dt
        
        
    #x1 = np.ravel(np.array([x1]))
    x2 = np.ravel(np.array([x2]))
    #y1 = np.ravel(np.array([y1]))
    y2 = np.ravel(np.array([y2]))
    
    return np.transpose(np.array([x1,x2,y1,y2]))
    


#Traject(Data[0,1],Data[0,2],Data[0,3],Data[0,4],Data[0,5],Data[1,1],Data[1,2],Data[1,3],Data[1,4],Data[1,5])
#Data = (Traject(Data))
#plt.plot(time,Data[:,0])
#plt.show()
